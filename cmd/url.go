package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"bitbucket.org/envimate/envibin-cli/domain"
	"bitbucket.org/envimate/envibin-go-client"
)

// urlCmd represents the url command
var urlCmd = &cobra.Command{
	Use:   "url",
	Short: "Get a presigned url for an artifact",
	Long: `Get a presigned url for an artifact for unauthenticated to download.`,
	Args: cobra.ExactArgs(1),
	PreRun: mustHaveConfig,
	Run: func(cmd *cobra.Command, args []string) {
		if byTagFlag && byVersionFlag {
			error("both by-tag and by-version are enabled")
		}

		var artifactKey = domain.NewArtifactKey(args[0])

		c, err := client.New(cfg.Repo.URL)
		if err != nil {
			error(err)
		}

		var url string
		if !byTagFlag && !byVersionFlag {
			url, err = c.GetPresignedUrl(artifactKey)
		} else if byTagFlag {
			url, err = c.GetPresignedUrlByTag(artifactKey)
		} else if byVersionFlag {
			url, err = c.GetPresignedUrlByVersion(artifactKey)
		}

		if err != nil {
			error(err)
		}

		fmt.Println(url)
	},
}

func init() {
	urlCmd.Flags().BoolVarP(&byTagFlag, "by-tag", "t", false, "Force to find artifact by tag")
	urlCmd.Flags().BoolVarP(&byVersionFlag, "by-version", "v", false, "Force to find artifact by version-number")
	rootCmd.AddCommand(urlCmd)
}
