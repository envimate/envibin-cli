package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"bitbucket.org/envimate/envibin-cli/domain"
	"bitbucket.org/envimate/envibin-go-client"
)

// removeCmd represents the remove command
var removeCmd = &cobra.Command{
	Use:   "remove [<artifact-name>(:<version-name>)]",
	Short: "Remove an artifact or version.",
	Long: `Remove an artifact or version by given artifact name (and version name).
Accepts artifact name or artifact key.
Artifact key exists of an artifact name and version number.
I.e. com.envimate.ubuntu-xenial:1.0`,
	Args: cobra.ExactArgs(1),
	PreRun: mustHaveConfig,
	Run: func(cmd *cobra.Command, args []string) {
		var artifactKey *domain.ArtifactKey

		artifactKey = domain.NewArtifactKey(args[0])

		c, err := client.New(cfg.Repo.URL)
		if err != nil {
			error(err)
		}

		err = c.Remove(artifactKey)
		if err != nil {
			error(err)
		}

		fmt.Println("Removed", artifactKey.String())
	},
}

func init() {
	rootCmd.AddCommand(removeCmd)
}
