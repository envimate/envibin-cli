package cmd

import (
	"github.com/spf13/cobra"
)

var forceFlag bool

// configCmd represents the config command
var configCmd = &cobra.Command{
	Use:   "config",
	Short: "Set configuration values",
	Long: `Set configuration values`,
}

func init() {
	configCmd.PersistentFlags().BoolVarP(&forceFlag, "force", "f", false, "Configuration will not be validated.")
	rootCmd.AddCommand(configCmd)
}
