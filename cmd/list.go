package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"strings"
	"bitbucket.org/envimate/envibin-go-client"
)

const versionHeader = "VERSION"
const artifactHeader = "ARTIFACT"
const tagsHeader = "TAGS"
const labelsHeader = "LABELS"
const timestampHeader = "TIMESTAMP"

// listCmd represents the ls command
var listCmd = &cobra.Command{
	Use:   "list [artifact name]",
	Short: "List versions",
	Long: `List versions`,
	Args: cobra.MaximumNArgs(1),
	PreRun: mustHaveConfig,
	Run: func(cmd *cobra.Command, args []string) {
		c, err := client.New(cfg.Repo.URL)
		if err != nil {
			error(err)
		}

		var versions []client.Version
		if len(args) == 0 {
			versions, err = c.GetLatestPerArtifact()
			if err != nil {
				error(err)
			}
		} else {
			var artifactName = args[0]
			if strings.Contains(artifactName, ":") {
				error("invalid artifact-name, should not contain version")
			}

			versions, err = c.GetVersions(artifactName, withTag, withLabel)
			if err != nil {
				error(err)
			}

			if len(versions) == 0 {
				error("No versions found for", artifactName)
			}
		}

		outputVersions(versions)

	},
}

func outputVersions(versions []client.Version) {
	var versionList = make([]string, 0, len(versions))
	var versionLength = 7
	var artifactLength = 9
	var tagsLength = 5
	var labelsLength = 7
	var timestampLength = 10

	for _, v := range versions {
		if len(v.VersionNumber) > versionLength { versionLength = len(v.VersionNumber) }
		if len(v.ArtifactName) > artifactLength { artifactLength = len(v.ArtifactName) }
		if len(strings.Join(v.Tags, ",")) > tagsLength { tagsLength = len(strings.Join(v.Tags, ",")) }
		if len(strings.Join(v.Labels, ",")) > labelsLength { labelsLength = len(strings.Join(v.Labels, ",")) }
		if len(v.Timestamp()) > timestampLength { timestampLength = len(v.Timestamp()) }
	}

	for _, v := range versions {
		var version = padRight(v.VersionNumber, " ", versionLength+1)
		var artifact = padRight(v.ArtifactName, " ", artifactLength+1)
		var tags = padRight(strings.Join(v.Tags, ","), " ", tagsLength+1)
		var labels = padRight(strings.Join(v.Labels, ","), " ", labelsLength+1)
		var timestamp = padRight(v.Timestamp(), " ", timestampLength+1)

		versionList = append(versionList, fmt.Sprint(version, artifact, tags, labels, timestamp))
	}

	outputVersionsHeader(versionLength, artifactLength, tagsLength, labelsLength, timestampLength)
	for _, str := range versionList {
		fmt.Println(str)
	}
}

func outputVersionsHeader(versionLength, artifactLength, tagsLength, labelLength, timestampLength int) {
	var version = padRight(versionHeader, " ", versionLength)
	var artifact = padRight(artifactHeader, " ", artifactLength)
	var tags = padRight(tagsHeader, " ", tagsLength)
	var labels = padRight(labelsHeader, " ", labelLength)
	var timestamp = padRight(timestampHeader, " ", timestampLength)

	fmt.Println(version, artifact, tags, labels, timestamp)
}

func padRight(str, pad string, length int) string {
	for {
		str += pad
		if len(str) > length {
			return str
		}
	}
}

func init() {
	listCmd.Flags().StringArrayVar(&withLabel, "with-label", []string{},
	"filters versions on labels, can be used multiple times.")
	listCmd.Flags().StringArrayVar(&withTag, "with-tag", []string{},
	"filters versions on tags, can be used multiple times.")
	rootCmd.AddCommand(listCmd)
}
