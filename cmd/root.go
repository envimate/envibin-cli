package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"bitbucket.org/envimate/config"
)

const programVersion = "0.0.1"
const keyRepoURL = "repo.url"
const versionUnknown = "UNKNOWN"

var cfg *config.Config

var binaryName = os.Args[0]
var withLabel []string
var withTag []string
var rmFlag bool
var cfgFlag string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   binaryName,
	Short: "Envibin command-line interface.",
	Long:  `Envibin command-line interface.`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func error(msg ...interface{}) {
	fmt.Println("Error:", msg)
	os.Exit(1)
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	rootCmd.PersistentFlags().StringVarP(&cfgFlag, "config", "c", "", "Use config using prefix prefix")
}

func initConfig() {

}

func mustHaveConfig(cmd *cobra.Command, args []string) {
	if cfgFlag == "" {
		err := config.Init()
		if err != nil {
			error(err)
		}
		cfg = config.Default
	} else {
		c, err := config.Get(cfgFlag)
		if err != nil {
			error(fmt.Sprintf("No configuration with prefix '%s' found.", cfgFlag))
		}
		cfg = c
	}

	// If a config file is found, read it in.
	if cfg == nil {
		fmt.Printf("No configuration found at %s\n", "~/.envibin/<config-prefix>-config.yml")
		fmt.Printf("Please configure the envibin cli first by running:" +
			" \n\n\t%s config --config=<config_prefix> repo.url <http://your.envibin.repo:9000>\n\n",
			binaryName)
		os.Exit(1)
	}
}
