# Envibin CLI

This command-line tool enables easy and convenient access to any envibin repository.
Use this tool to list, put, get or delete binary files using the command-line.

## Installation
##### From source
Currently it can only be compiled and installed from source.
```
go get -u bitbucket.org/envimate/envibin-cli
cd $GOPATH/src/bitbucket.org/envimate/envibin-cli
go install
```

##### Configuring a repository
The tool needs to know which Envibin repository to communicate with. To set the default configuration use:
```
envibin-cli config repo.url <url-to-your-repo>
```
Run `envibin-cli version` to check if the configured repository url is available.
This will have created a configuration file in your `~/.envibin` directory called `default.yaml`.

## Usage

##### Putting a binary
Let's say you have a file called `ubuntu-xenial.box` that you want to version in Envibin. And it'll be the first of its kind.
Let's first configure and see if our repository is up and running:
```
$ envibin-cli config repo.url https://envimate.com:9000
$ envibin-cli version
envibin-cli 0.0.1
envibin-repository 0.8 UP
```
Now we know everything is properly set up, our new repository will likely be empty:
```
$ envibin-cli list
VERSION  ARTIFACT   TAGS   LABELS   TIMESTAMP
```
This command will show all the last added version for each artifact in envibin. Obviously, it shows up empty now.
To change that, let's version the binary as `com.envimate.ubuntu-xenial` in Envibin. This will be version `1.0`. We'll also tag it `latest` for added clearity.
```
$ envibin-cli put com.envimate.ubuntu-xenial:1.0 ubuntu-xenial.box --with-tag latest
Created com.envimate.ubuntu-xenial:1.0
$ envibin-cli list
VERSION  ARTIFACT                    TAGS    LABELS   TIMESTAMP
1.0      com.envimate.ubuntu-xenial  latest           2017-12-11 17:48:34
```
The `list` operation can also be used to list versions for a specific artifact. I'll add some more versions for demonstration purposes.
```
$ envibin-cli put com.envimate.ubuntu-xenial:1.1 ubuntu-xenial.box --with-tag latest
Created com.envimate.ubuntu-xenial:1.1
$ envibin-cli put com.envimate.ubuntu-xenial:1.2 ubuntu-xenial.box --with-tag latest
Created com.envimate.ubuntu-xenial:1.2
$ envibin-cli put com.envimate.ubuntu-xenial:1.3 ubuntu-xenial.box --with-tag latest
Created com.envimate.ubuntu-xenial:1.3
$ envibin-cli list
VERSION  ARTIFACT                    TAGS    LABELS   TIMESTAMP
1.3      com.envimate.ubuntu-xenial  latest           2017-12-11 17:52:28
$ envibin-cli list com.envimate.ubuntu-xenial
VERSION  ARTIFACT                    TAGS    LABELS   TIMESTAMP
1.0      com.envimate.ubuntu-xenial                   2017-12-11 17:48:34
1.1      com.envimate.ubuntu-xenial                   2017-12-11 17:52:21
1.1      com.envimate.ubuntu-xenial                   2017-12-11 17:52:21
1.3      com.envimate.ubuntu-xenial  latest           2017-12-11 17:52:28
```
Also note a tag can only be used once amongst versions of the same artifact. When tagging a version with i.e. `latest`, any previous version will be untagged. For labels that is not the case; i.e. multiple versions can have the label `build-success`. To tag or label already existing versions, use the `tag` or `label` command.

##### Removing artifacts and versions
Removing an artifact or version is just as simple. Let's say we want to remove the oldest version `1.0`.
```
$ envibin-cli list com.envimate.ubuntu-xenial
VERSION  ARTIFACT                    TAGS    LABELS   TIMESTAMP
1.0      com.envimate.ubuntu-xenial                   2017-12-11 17:48:34
1.1      com.envimate.ubuntu-xenial                   2017-12-11 17:52:21
1.1      com.envimate.ubuntu-xenial                   2017-12-11 17:52:21
1.3      com.envimate.ubuntu-xenial  latest           2017-12-11 17:52:28
$ envibin-cli remove com.envimate.ubuntu-xenial:1.0
Removed com.envimate.ubuntu-xenial:1.0
$ envibin-cli list com.envimate.ubuntu-xenial
VERSION  ARTIFACT                    TAGS    LABELS   TIMESTAMP
1.1      com.envimate.ubuntu-xenial                   2017-12-11 17:52:21
1.1      com.envimate.ubuntu-xenial                   2017-12-11 17:52:21
1.3      com.envimate.ubuntu-xenial  latest           2017-12-11 17:52:28
```

To remove all versions of an artifact, run the same command without a version-name. I.e.

```
$ envibin-cli remove com.envimate.ubuntu-xenial

```

##### Downloading version binaries
The point of storing binaries using artifacts and versions is to eventually be able to pull them out again. Usually this is done by the provided application integrations such as for Vagrant or Docker. But envibin-cli also provides the `get` command which let's you download binaries.
```
$ envibin-cli get com.envimate.ubuntu-xenial:1.3
```
This command downloads the binaries and stores them in the current directory using the artifact-name and version-number. Or the `--output` flag can be used to specify a different location.
Ofcourse also tag-names can be used to get binaries, the name of the file will then be defaulted to include the tag-name instead of the version-number.
```
$ envibin-cli get com.envimate.ubuntu-xenial:latest
```

## Quick Reference
The `--help` flag should help you find your way using this tool. Some example commands:
```
envibin-cli version
envibin-cli list
envibin-cli list com.your.artifact
envibin-cli list com.your.artifact --with-tag latest --with-label test-succeed --with-label build-succeed
envibin-cli put com.your.artifact:1.1 your-binary.file --with-tag latest
envibin-cli get com.your.artifact:1.1
envibin-cli get com.your.artifact:1.1 --output target-file.name
envibin-cli label com.your.artifact:1.1 new-label-name-1 new-label-name-2
envibin-cli tag com.your.artifact:1.1 new-tag-name-1 new-tag-name-2
envibin-cli remove com.your.artifact
envibin-cli remove com.your.artifact:1.1
```