package config

import (
	"github.com/spf13/viper"
	"os"
	"gopkg.in/yaml.v2"
)

//Config reflects the content of the configuration file.
type Config struct {
	Repo Repo
}

//Repo reflects the Envibin repository configuration.
type Repo struct {
	URL string
}

//ReadConfig reads configuration from the configured viper config file location.
func ReadConfig() (*Config, error) {
	var c *Config
	err := viper.Unmarshal(&c)

	return c, err
}

//SaveConfig creates updated configuration at the configured viper config file location.
//Any old configuration will be overwritten.
func SaveConfig(cfgpath string, config *Config) error {
	b, err := yaml.Marshal(config)
	if err != nil {
		return err
	}

	_ = os.Remove(cfgpath)
	f, err := os.Create(cfgpath)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.Write(b)
	return err
}