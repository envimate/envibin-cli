package cmd

import (
	"github.com/spf13/cobra"
	"bitbucket.org/envimate/envibin-cli/domain"
	"bitbucket.org/envimate/envibin-go-client"
)

var (
	outputFlag string
	byTagFlag bool
	byVersionFlag bool
)

// getCmd represents the pull command
var getCmd = &cobra.Command{
	Use:   "get [<artifact-name>:<version-name>]",
	Short: "Get an artifact binary.",
	Long: `Get an artifact binary by the given artifact-key.
An artifact key contains the artifact name and version number seperated by ':'.
I.e.: com.envimate.ubuntu-xenial:1.0`,
	Args: cobra.ExactArgs(1),
	PreRun: mustHaveConfig,
	Run: func(cmd *cobra.Command, args []string) {
		if byTagFlag && byVersionFlag {
			error("--by-tag and --by-version are both enabled")
		}

		var artifactKey = domain.NewArtifactKey(args[0])

		if artifactKey.IsInvalid() {
			error("invalid artifact key", artifactKey.String())
		}

		c, err := client.New(cfg.Repo.URL)
		if err != nil {
			error(err)
		}

		if !byTagFlag && !byVersionFlag {
			err = c.Pull(artifactKey, outputFlag)
			if err != nil {
				error(err)
			}
		} else if byTagFlag {
			err = c.PullByTag(artifactKey, outputFlag)
			if err != nil {
				error(err)
			}
		} else if byVersionFlag {
			err = c.PullByVersion(artifactKey, outputFlag)
			if err != nil {
				error(err)
			}
		}
	},
}

func init() {
	getCmd.Flags().StringVarP(&outputFlag, "output", "o", "", "download binary to output location")
	getCmd.Flags().BoolVarP(&byTagFlag, "by-tag", "t", false, "force to download as if a tag was given")
	getCmd.Flags().BoolVarP(&byVersionFlag, "by-version", "v", false, "force to download as if a version-number was given")
	rootCmd.AddCommand(getCmd)
}
