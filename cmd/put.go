package cmd

import (
	"github.com/spf13/cobra"
	"bitbucket.org/envimate/envibin-cli/domain"
	"fmt"
	"bitbucket.org/envimate/envibin-go-client"
)

// putCmd represents the push command
var putCmd = &cobra.Command{
	Use:   "put [<artifact-name>:<version-name>] [file]",
	Short: "Put a binary",
	Long: `Put a binary by the given artifact key.
An artifact key contains the artifact name and version number seperated by ':'.
I.e.: com.envimate.ubuntu-xenial:1.0`,
	Args: cobra.ExactArgs(2),
	PreRun: mustHaveConfig,
	Run: func(cmd *cobra.Command, args []string) {
		var artifactKey *domain.ArtifactKey
		var filePath string

		artifactKey = domain.NewArtifactKey(args[0])
		filePath = args[1]

		if artifactKey.IsInvalid() {
			error("invalid artifact key", artifactKey.String())
		}

		c, err := client.New(cfg.Repo.URL)
		if err != nil {
			error(err)
		}

		err = c.Push(artifactKey, filePath, withTag, withLabel)
		if err != nil {
			error(err)
		}

		fmt.Println("Created", artifactKey.String())
	},
}

func init() {
	putCmd.Flags().
		StringArrayVar(&withLabel, "with-label", []string{}, "sets a label, can be used multiple times.")
	putCmd.Flags().
		StringArrayVar(&withTag, "with-tag", []string{}, "sets a tag, can be used multiple times.")
	rootCmd.AddCommand(putCmd)
}
