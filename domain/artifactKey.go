package domain

import "strings"

type ArtifactKey struct {
	Name    string
	Version string
}

func (key *ArtifactKey) IsInvalid() bool {
	if key.Name == "" {
		return true
	}
	if key.Version == "" {
		return true
	}

	return false
}

func (key *ArtifactKey) String() string {
	if key.Version == "" {
		return key.Name
	} else {
		return key.Name + ":" + key.Version
	}
}

func NewArtifactKey(key string) *ArtifactKey {
	artifactKey := &ArtifactKey{}

	parts := strings.Split(key, ":")
	if len(parts) > 0 {
		artifactKey.Name = parts[0]
	}
	if len(parts) > 1 {
		artifactKey.Version = parts[1]
	}

	return artifactKey
}
