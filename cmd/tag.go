package cmd

import (
	"github.com/spf13/cobra"
	"bitbucket.org/envimate/envibin-cli/domain"
	"bitbucket.org/envimate/envibin-go-client"
)

// tagCmd represents the tag command
var tagCmd = &cobra.Command{
	Use:   "tag [<artifact-name>:<version-name>] [tags...]",
	Short: "Tag an existing artifact.",
	Long: `Tag an existing artifact by the given artifact key with the given tag(s).
An artifact key contains the artifact name and version number seperated by ':'.
I.e.: com.envimate.ubuntu-xenial:1.0`,
	Args: cobra.MinimumNArgs(2),
	PreRun: mustHaveConfig,
	Run: func(cmd *cobra.Command, args []string) {
		var artifactKey *domain.ArtifactKey
		var tags []string

		artifactKey = domain.NewArtifactKey(args[0])
		tags = args[1:]

		if artifactKey.IsInvalid() {
			error("invalid artifact key", artifactKey.String())
		}

		c, err := client.New(cfg.Repo.URL)
		if err != nil {
			error(err)
		}

		if rmFlag {
			err = c.RemoveTag(artifactKey, tags)
			if err != nil {
				error(err)
			}
		} else {
			err = c.Tag(artifactKey, tags)
			if err != nil {
				error(err)
			}
		}

	},
}

func init() {
	tagCmd.Flags().BoolVar(&rmFlag, "rm", false, "will remove the tags instead")
	rootCmd.AddCommand(tagCmd)
}
