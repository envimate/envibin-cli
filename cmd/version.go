package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"bitbucket.org/envimate/envibin-go-client"
)

// versionCmd represents the version command
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Show version information.",
	Long: `Show version information.`,
	PreRun: mustHaveConfig,
	Run: func(cmd *cobra.Command, args []string) {
		var repoVersion string
		var repoStatus string

		c, err := client.New(cfg.Repo.URL)
		if err != nil {
			repoVersion = versionUnknown
			repoStatus = ""
		}

		health, err := c.GetHealth()
		if err != nil {
			repoVersion = versionUnknown
			repoStatus = ""
		} else {
			repoVersion = health.Version
			repoStatus = health.Status
		}

		fmt.Println("envibin-cli", programVersion)
		fmt.Println("envibin-repository", repoVersion, repoStatus)
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
}
