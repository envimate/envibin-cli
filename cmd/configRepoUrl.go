package cmd

import (
	"github.com/spf13/cobra"
	"fmt"
	"net/url"
	"bitbucket.org/envimate/envibin-go-client"
	"bitbucket.org/envimate/config"
)

// repo.urlCmd represents the repo.url command
var repoURLCmd = &cobra.Command{
	Use:   "repo.url [url]",
	Short: "The base URL of the envibin repository",
	Long:  `The base URL of the envibin repository. For example: https://your.repo.com:9000/`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		var urlArg = args[0]

		if cfgFlag == "" {
			err := config.Init()
			if err != nil {
				error(err)
			}
			cfg = config.Default
		} else {
			c, err := config.Get(cfgFlag)
			if err != nil {
				fmt.Printf("Creating configuration file for prefix '%s'\n", cfgFlag)
				c, err = config.Create(cfgFlag)
				if err != nil {
					error(err)
				}
				cfg = c
			} else {
				cfg = c
			}
		}

		if !forceFlag {
			if !isValidURL(urlArg) {
				error("invalid url", urlArg)
			}

			if !isReachableURL(urlArg) {
				error("could not reach url", urlArg)
			}
		}

		fmt.Println("config changed:\n", cfg.Repo.URL, "to\n", urlArg)
		cfg.Repo.URL = urlArg

		cfg.Save()
	},
}

func isValidURL(value string) bool {
	_, err := url.ParseRequestURI(value)
	return err == nil
}

func isReachableURL(value string) bool {
	c, err := client.New(value)
	if err != nil {
		return false
	}

	health, err := c.GetHealth()
	if err != nil {
		return false
	}

	if health.Status != "UP" {
		return false
	}

	return true
}

func init() {
	configCmd.AddCommand(repoURLCmd)
}
